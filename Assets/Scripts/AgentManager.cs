using System;
using System.Collections.Generic;
using System.Collections;
using Unity.MLAgents;
using Unity.MLAgents.Policies;
using Unity.MLAgents.Sensors;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;


/// <summary>
/// Controlls the behaviour of the agent. Collects observations, adds rewards, and performs actions. 
/// </summary>
public class AgentManager : Agent
{
    public EnergyBar energyBar;
    public HappinessBar happinessBar;
    public WaterBar waterBar;
    public GameObject eatingBubble;
    public AreaManager areaManager;
    new private Rigidbody rigidbody;
    new private Collider collider;
    new private Animator animation;
    private BehaviorParameters behavior;
    public float[] homeostaticValues;
    private float energyLastStep;
    public string[] parents;
    public int id;
    public bool immortal;
    public bool ruleBased;
    public float rotationForce;
    public float metabolism;
    public float moveForce;
    private GameObject eatObj;
    public Specie specie;
    // TODO: Energy should be set on birth (maybe not always full)
    public float energy;
    public float timeUntilFertile = 0.0f;
    private float reproductionReward = 1;
    // Probability of giving birth to new animat when colliding
    private float reproductionProbability = 1;

    private List<Tuple<float, float>> smellVectors = new List<Tuple<float, float>>();
    private List<float> listOfHappinesses = new List<float>();
    private double happiness = 0;
    private double happinessPrev;
    private List<float> smellMagnitudesPrev = new List<float>();
    public float lifetime = 0;
    public float foodFreq = 0;
    private int happinessCounterLength = 100;
    public bool isDrinking = false;
    protected enum AgentActions
    {
        walk,
        rotate,
        eat
    }
    public override void Initialize()
    {
        rigidbody = GetComponent<Rigidbody>();
        areaManager = GetComponentInParent<AreaManager>();
        collider = GetComponent<Collider>();
        animation = GetComponent<Animator>();
        behavior = GetComponent<BehaviorParameters>();
        energyBar = GetComponentInChildren<EnergyBar>();
        happinessBar = GetComponentInChildren<HappinessBar>();
        waterBar = GetComponentInChildren<WaterBar>();
    }

    public void InitializeSmellVectors()
    {
        foreach (string smell in areaManager.smellables)
        {

            float vec = 0;
            float angle = 0;
            smellVectors.Add(Tuple.Create(angle, vec));
            smellMagnitudesPrev.Add(0f);
        }
    }
    public void InitializeHappinessList()
    {
        for (int i = 0; i < happinessCounterLength; i++)
        {
            listOfHappinesses.Add(0.0f);
        }
    }

    public void SetParameters(Vector3 pos, Specie agent_specie, string[] parentNames, int agentsCreated)
    {
        transform.position = pos;
        specie = agent_specie;
        moveForce = specie.moveForce;
        rotationForce = specie.rotationForce;
        metabolism = specie.metabolism;

        // Kind of ugly, creating new local array which the public is then referencing to... 
        float[] homeostatics = new float[specie.homeostaticNames.Length];
        // Assign each homeostatic variable a value of 1
        for (int i = 0; i < specie.homeostaticNames.Length; i++)
        {
            homeostatics[i] = 1;
        }
        homeostaticValues = homeostatics;
        parents = parentNames;
        id = agentsCreated;

        if (specie.immortal)
        {
            specie.metabolism = 0;
        }
    }
    public void SetBehaviorName(string behaviorName)
    {
        // behavior.BehaviorName = behaviorName;
    }

    // Spawn an agent
    public void Spawn(Vector3 pos)
    {
        if (specie.specieName == "deer")
        {
            areaManager.totalNrOfPrey += 1;
        }
        if (specie.specieName == "wolf")
        {
            areaManager.totalNrOfPredators += 1;
        }
        foodFreq = 0;
        transform.position = pos;
        transform.rotation = UnityEngine.Random.rotation;
    }

    // Update the homeostatic variables and change UI
    private void DeltaHomeostatic(float walk, float rotate)
    {   
        float movementMetabolism=0;
        if (walk==1 || rotate != 0)
        {
            movementMetabolism = 1;
        }
        if (walk==2)
        {
            movementMetabolism = 4;
        }
        // Assumption, water and energy are equally affected by time and motion
        for (int i = 0; i < homeostaticValues.Length; i++)
        {
            homeostaticValues[i] -= (specie.metabolism + (specie.metabolism * movementMetabolism)) * Time.deltaTime;
            if(i==0)
            {
                energyBar.SetEnergyBar(homeostaticValues[i]);
            }
            if(i==1)
            {
                waterBar.SetWaterBar(homeostaticValues[i]);
            }
        }
    }
    // Check in on the homeostatic status and do actions accordingly
    public void CheckHomeostatic()
    {
        foreach (float homeostatic in homeostaticValues)
        {
            if (homeostatic < 0)
            {
                if (specie.specieName == "deer")
                {
                    areaManager.starvedPrey += 1;
                }
                if (specie.specieName == "deer5")
                {
                    areaManager.starvedPrey5 += 1;
                }
                if (specie.specieName == "deer6")
                {
                    areaManager.starvedPrey6 += 1;
                }
                if (specie.specieName == "wolf")
                {
                    areaManager.starvedPredators += 1;
                }
                Kill();
            }
        }
    
    }

    public void FixedUpdate()
    {
        // If it drinks and the homeostatic variable Water is used
        lifetime += Time.fixedDeltaTime;

        if (isDrinking && homeostaticValues.Length > 1)
        {
            homeostaticValues[1] = 1;
        }
    }

    public void ResetHomeostatic()
    {
        for (int i = 0; i < homeostaticValues.Length; i++)
        {
            if (areaManager.randomizeHomeostatics)
            {
                homeostaticValues[i] = UnityEngine.Random.value;
            }
            else
            {
                homeostaticValues[i] = 1f;
            }
            // Set just above dead to make them hungry
            if (immortal)
            {
                homeostaticValues[i] = 0.01f;
            }
        }
        
    }

    public void Move(float walk, float rotate)
    {
        // Find current rotation of agent
        Vector3 newRot = rigidbody.rotation.eulerAngles;
        newRot.z = 0;
        newRot.x = 0;
        rigidbody.rotation = Quaternion.Euler(newRot);
        
        if (isDrinking)
        {
            // Cant run when drinking, to avoid animat from running over the water through the collider
            if (walk == 2)
            {
                walk = 1;
            }
        }
        // Create translation vector and rotation with respect to actions
        float translation = walk * moveForce;
        float rotation = rotate * rotationForce;
        translation *= Time.fixedDeltaTime;
        rotation *= Time.fixedDeltaTime;
        Quaternion turn = Quaternion.Euler(0f, rotation, 0f);
        // Move with respect to current position and translation
        rigidbody.MovePosition(rigidbody.position + this.transform.forward * translation);
        rigidbody.MoveRotation(rigidbody.rotation * turn);

        if (Math.Abs(walk) == 1)
        {
            animation.Play("walk");
        }
        if (Math.Abs(walk) == 2)
        {
            animation.Play("run");
        }
    }

    public void Eating(bool eat)
    {
        if (eat)
        {
            animation.Play("eat");
            // Create eating object during eating that can collide with food 
            eatObj = Instantiate(eatingBubble, transform);
            // Destroy it immediately after
            Destroy(eatObj);
        }
    }


    // Kill the agent (remove)
    public void Kill()
    {
        animation.Play("die");
        if (specie.respawn)
        {
            AddReward(specie.deathReward);
            EndEpisode();
        }
        else
        {
            EndEpisode();
            areaManager.RemoveAgent(this);
        }
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        // If animat is eating, we have to wait until it is finished.
        // if (animation.GetCurrentAnimatorStateInfo(0).IsName("eat"))
        // {
        //     return;
        // }

        float walk = vectorAction[0];

        // From [0,1,2] to [-1,0,1] since inputs from network is only positive integers
        float rotate = 0;
        if (vectorAction[1] == 0)
        {
            rotate = 0;
        }
        if (vectorAction[1] == 1)
        {
            rotate = -1;
        }
        if (vectorAction[1] == 2)
        {
            rotate = 1;
        }

        //? Not using the eat action atm.
        // bool eat = Convert.ToBoolean(vectorAction[2]);
        // Dont kill if frozen body
        if (rigidbody.constraints != RigidbodyConstraints.FreezePosition)
        {
            DeltaHomeostatic(walk, rotate);
            CheckHomeostatic();
            Move(walk, rotate);
        }

        // Eating(eat);
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        List<Tuple<float, float>> smellVectors = ObserveSmells();
        HomeostaticNetwork(smellVectors, sensor);

        //! If using reproduction
        // Add normalized time of how long until fertile or 0 if fertile
        // sensor.AddObservation(Math.Max(0, timeUntilFertile/30));

    }

    public void GiveEnergyNeighborWolves()
    {
        foreach(AgentManager animat in areaManager.Agents)
        {
            // If a wolf
            if (animat.specie.specieName == "wolf" )
            {
                // If within observaitonradius
                if (Vector3.Distance(transform.position, animat.transform.position) < animat.specie.observationradius/2)
                {
                    // Also give energy (sharing the prey)
                    animat.homeostaticValues[0] = Math.Min(animat.homeostaticValues[0] + 0.5f, 1);
                }
               
            }
        }
    }
    public void OnTriggerEnter(Collider collider)
    {
        // if this agent is a predator
        if (collider.gameObject.tag == "wall")
        {
            
            // AddReward(-1);
        
        }
        if (this.gameObject.tag == "predator")
        {
            AgentManager animat = collider.GetComponentInParent<AgentManager>();

            // and it collides with a 
            if (collider.gameObject.tag == "prey")
            {
                // GiveEnergyNeighborWolves();
                animation.Play("attack");
                // Reward for killing
                AddReward(specie.eatReward);
                // Add counter of prey killed
                foodFreq += 1;
                // Kill the prey
                if (animat.specie.specieName == "deer")
                {
                    areaManager.predatedPrey += 1;
                }
                if (animat.specie.specieName == "deer5")
                {
                    areaManager.predatedPrey5 += 1;
                }
                if (animat.specie.specieName == "deer6")
                {
                    areaManager.predatedPrey6 += 1;
                }
                animat.Kill();
                // rigidbody.constraints = RigidbodyConstraints.FreezePosition;
                // Assuming energy on first index in homeostatic
                homeostaticValues[0] = Math.Min(homeostaticValues[0] + 1, 1);
                // yield return new WaitForSeconds(2.0f);
                animation.Play("eat");
                // rigidbody.constraints = RigidbodyConstraints.None;
            }
            // if it collides with another predator
            // if (collider.gameObject.tag == "predator" && areaManager.reproduceSexually && BothParentsFertile(animat))
            // {
            //     // if the two colliding animats are of same specie
            //     if (animat.specie.specieName == specie.specieName)
            //     {
            //         AddReward(reproductionReward);
            //         ReproduceSexual();
            //     }
            // }

        }
        // if (this.gameObject.tag == "prey")
        // {
        //     AgentManager animat = collider.GetComponentInParent<AgentManager>();

        //     if (collider.gameObject.tag == "prey" && areaManager.reproduceSexually && BothParentsFertile(animat))
        //     {
        //         // Set 30 seconds until it can ReproduceSexual again
        //         timeUntilFertile = 30;
        //         // if the two colliding animats are of same specie
        //         if (animat.specie.specieName == specie.specieName)
        //         {
        //             AddReward(reproductionReward);
        //             ReproduceSexual();
        //         }
        //     }
        // }
    }

    // Checks if both parents are available for reproduction
    public bool BothParentsFertile(AgentManager animat)
    {
        if (animat.timeUntilFertile < 0 && timeUntilFertile < 0)
        {
            return true;
        }
        else { return false; }
    }

    public void ReproduceSexual()
    {
        float random = UnityEngine.Random.value;

        // Initialize new agent of same specie with probability.
        if (random < reproductionProbability)
        {
            areaManager.GiveBirthSexual(specie, gameObject.transform.position);
        }
    }

    public void Update()
    {
        // timeUntilFertile -= Time.deltaTime;
        // lifetime += Time.fixedDeltaTime;

    }

    public override void OnEpisodeBegin()
    {
        // Reset agent
        ResetAgent();

    }
    private void ResetAgent()
    {
        ResetHomeostatic();
        Vector3 randomSpawnPos = areaManager.GetClosestFreePosition(areaManager.GetRandomSpawnPos());
        Spawn(randomSpawnPos);
    }

    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = 0;
        actionsOut[1] = 0;
        // actionsOut[2] = 0;
        if (Input.GetKey(KeyCode.W))
        {
            actionsOut[0] = 1;

        }
        if (Input.GetKey(KeyCode.Q))
        {
            actionsOut[0] = 2;

        }
        if (Input.GetKey(KeyCode.D))
        {
            actionsOut[1] = 2;
        }
        if (Input.GetKey(KeyCode.S))
        {
            actionsOut[0] = 0;
        }
        if (Input.GetKey(KeyCode.A))
        {
            actionsOut[1] = 1;
        }
        // Eat if G
        // if (Input.GetKey(KeyCode.G))
        // {
        //     actionsOut[2] = 1;
        // }

    }

    public List<Tuple<float, float>> ObserveSmells()
    {
        // If first time smelling, create initial vectors to be compared with to decide if increasing magnitude
        if (!smellVectors.Any())
        {
            InitializeSmellVectors();
        }

        // Loop over all smellables and find the smells
        int i = 0;
        foreach (string smell in areaManager.smellables)
        {
            smellVectors[i] = GetSmellVector(smell);
            i++;
        }
        return smellVectors;
    }

    private Tuple<float, float> GetSmellVector(string smell)
    {
        Vector3 obj;
        float smellMagnitude;
        float relativeAngle;
        (relativeAngle, obj) = GetNearest(smell);

        smellMagnitude =  (float)Math.Pow(((specie.observationradius - obj.magnitude)/specie.observationradius), 2);

        return Tuple.Create(relativeAngle, smellMagnitude);

    }

    // Get relative angle and distance to nearest object with "tag", if it is within observation radius
    public Tuple<float, Vector3> GetNearest(string tag)
    {
        // Find all gameobjects with tag and sort according to relative distance to agent
        GameObject[] objects = GameObject.FindGameObjectsWithTag(tag);
        System.Array.Sort<GameObject>(objects, (g1, g2) => (Vector3.Distance(transform.position, g1.transform.position) < Vector3.Distance(transform.position, g2.transform.position)) ? -1 : 1);


        Vector3 agentRot = rigidbody.rotation.eulerAngles;
        // Some point damn far away
        Vector3 zerovec = new Vector3(9999999.0f, 9999999.0f, 9999999.0f);
        Vector3 vec = new Vector3(9999999.0f, 9999999.0f, 9999999.0f);
        float relativeAngle = 0.0f;
        if (objects.Length == 0)
        {
            return Tuple.Create(relativeAngle, zerovec);
        }
        else
        {
            foreach (GameObject obj in objects)
            {
                // Ignore object if it is itself
                if (obj == this.gameObject)
                {
                    return Tuple.Create(relativeAngle, zerovec);
                }
                // Get vector relative to agent
                vec = obj.transform.position - transform.position;
                // If object to far away we return the "zero" observation
                if (vec.magnitude > specie.observationradius)
                {
                    return Tuple.Create(relativeAngle, zerovec);
                }
                else
                {
                    // Rotate vector relative to agents rotation
                    relativeAngle = Vector3.Angle(transform.forward, vec);
                    return Tuple.Create(relativeAngle, vec);
                }
                
            }
        }
        return Tuple.Create(relativeAngle, vec);
    }

    public void HomeostaticNetwork(List<Tuple<float, float>> smellVectors, VectorSensor sensor)
    {
        happinessPrev = happiness;
        happiness = 0;

        int i = 0;
        // Add happiness for each smell
        foreach (Tuple<float, float> smellTuple in smellVectors)
        {
            float smellMagnitude = smellTuple.Item2;
            float relativeAngle = smellTuple.Item1;
            smellMagnitude = Math.Min(smellMagnitude, 1);
            float smellDifference = smellMagnitude - smellMagnitudesPrev[i];
            smellDifference = Math.Min(smellDifference, 1);
            sensor.AddObservation(smellMagnitude);
            sensor.AddObservation(smellDifference);
            sensor.AddObservation(relativeAngle/180f);

        //     // Check whether current smell is an eatable for this animat
            if (specie.eatables.Contains(areaManager.smellables[i]))
            {
                happiness += (float)(0.1f*smellMagnitude)*(1-(float)(homeostaticValues[0]));
                
            //     // Maybe not beautiful solution. When an object is for example eaten by the animat, the smell suddenly changes by a lot, we dont want to add a negative reward when this happens. Therefore, we compensate by adding a reward equal to but with opposite sign to what we just added to the happiness. If the smell of food suddenly got smaller, meaning that the smellDifference is large and negative, we add a positive reward of equal size. This way we should never get a net reward for this sudden smell differencies.
                if (Math.Abs(smellDifference)>0.01)
                {
                    AddReward((float)(0.1f*smellMagnitude)*(1-(float)(homeostaticValues[0])));
                }

            }
            if (specie.drinkables.Contains(areaManager.smellables[i]))
            {
                happiness += (float)(0.1f*smellMagnitude)*(1-(float)(homeostaticValues[1]));
                
                if (Math.Abs(smellDifference)>0.01)
                {
                    AddReward((float)(0.1f*smellMagnitude)*(1-(float)(homeostaticValues[1])));
                }
            }
            if (specie.predators.Contains(areaManager.smellables[i]))
            {
                happiness += (-0.1f*smellMagnitude);    
                if (Math.Abs(0.1f*smellDifference)>0.01)
                {
                    AddReward((float)(smellMagnitude));
                }
            }

            smellMagnitudesPrev[i] = smellMagnitude;
            i++;
        
        }
        // Add happiness for each homeostatic variable
        foreach (float homeostatic in homeostaticValues)
        {
            sensor.AddObservation(homeostatic);
            happiness += Math.Log(1 + 10*homeostatic)/(Math.Log(11));
        }
        float difference = (float)(happiness - happinessPrev);

        if(difference>0)
        {
            AddReward(2*difference);
        }
        if(difference<=0)
        {
            AddReward(difference);
        }
        happinessBar.SetHappinessBar((float)happiness);
    }

    public float GetLifetime()
    {
        return lifetime;
    }
    public float GetFoodFreq()
    {
        if (lifetime > 1)
        {
            return foodFreq;
        }
        else
        {
            return 0;
        }
    }
}

