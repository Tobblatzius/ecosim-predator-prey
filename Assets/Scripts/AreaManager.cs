using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Unity.MLAgents;
using Unity.MLAgents.SideChannels;
using System.Linq;
using UnityEngine.AI;


[Serializable]
public class Specie
{
    public int nrOnInit;
    public string specieName;
    public string tag;
    public float moveForce;
    public float rotationForce;
    public float metabolism;
    public int adultAge;
    public int nrAlive;
    public int observationradius;
    public float eatReward;
    public float deathReward;
    public bool immortal;
    // Whether agent should respawn through multiple lives or die 4-ever
    public bool respawn;

    [Tooltip("The names of the homeostatic variables of this specie")]
    public string[] homeostaticNames;

    public string[] parentNames;

    public string trainingModel;
    [Tooltip("List of object tags that this specie can eat")]
    public List<string> eatables = new List<string>();
    [Tooltip("List of object tags that this specie can drink")]
    public List<string> drinkables = new List<string>();
    [Tooltip("List of object tags that this specie can be eaten by")]
    public List<string> predators = new List<string>();

    public float reproductionProbability;

    public GameObject prefab;
}
/// <summary>
/// Controll for the game area. Provides global information and keeps track of all objects in the area. 
/// </summary>
public class AreaManager : MonoBehaviour
{
    [SerializeField]
    public List<Specie> Species = new List<Specie>();
    public List<string> smellables = new List<string>();


    [HideInInspector]
    // public InfoToPythonSideChannel toPython;

    StatsRecorder m_Recorder;
    protected SimulationInfoCanvas simInfo;
    private int agentsCreated = 0;
    public float homeostaticBirthThreshold;
    // To prohibit each parent creating one kid each. Make one kid for every to succesfull attempts. Init to 2 to make first attempt succesful
    private int onlyBirthOnEvenNumbers = 2;
    private bool spawnerSet = false;
    protected WorldManager worldManager;
    protected GameObject environment;
    protected FoodSpawner foodSpawner;
    protected WaterSpawner waterSpawner;
    public bool reproduceSexually;
    public bool reproduceAsexually;
    public bool mutations;
    public bool ecosystem1;
    public bool cantGoExtinct;
    public bool randomizeHomeostatics;
    public float preyCarryingCapacity;
    public float mutationConstant;
    public float envRadius;
    public int boardSize;
    public float foodCarryingCapacity;
    public int totalAgentsOnInit=0;
    private bool inializationDone = false;
    private int nPredators;
    private int nPrey;
    private int nPredatorsAdult;
    private int nPreyAdult;
    private int nFood;
    private int nWater;
    public float starvedPredators=0;
    public float predatedPrey=0;
    public float starvedPrey=0;
    public float predatedPrey5=0;
    public float starvedPrey5=0;
    public float predatedPrey6=0;
    public float starvedPrey6=0;
    public float totalNrOfPrey=0;
    public float totalNrOfPredators=0;
    private float avgPreyLifetime;
    private float avgPredatorLifeTime;
    private float avgFoodCollected;
    private float avgPreyKilled;
    private int nFixedUpdateFrames = 0;
    public List<Agent> Agents { get; private set; } = new List<Agent>();

    public void Initialize()
    {
        worldManager = GetComponentInParent<WorldManager>();
        environment = worldManager.environmentPrefab;
    }

    private void Awake()
    {
        // toPython = new InfoToPythonSideChannel();
        // SideChannelsManager.RegisterSideChannel(toPython);
        InitializeSpecies(Species);
        m_Recorder = Academy.Instance.StatsRecorder;
    }

    public void InitializeSpecies(List<Specie> Species)
    {

        foreach(Specie specie in Species)
        {
            InitializeAgents(specie);
        }
        inializationDone = true;
    }
    public void InitializeAgents(Specie specie)
    {

        for (int i = 0; i < specie.nrOnInit; i++)
        {
            Vector3 spawnpos = GetRandomSpawnPos();
            InitializeAgent(specie, spawnpos);
        }
    }
    public void InitializeAgent(Specie specie, Vector3 position)
    {
        AgentManager newAgent = Instantiate(specie.prefab, transform).GetComponent<AgentManager>();
        Agents.Add(newAgent);
        agentsCreated += 1;
        newAgent.SetParameters(position, specie, specie.parentNames, agentsCreated);
        
        if (mutations && inializationDone)
        {
            newAgent.moveForce = Mutate(GetAvgMoveForce(newAgent.specie.tag));
            newAgent.metabolism = Mutate(GetAvgMetabolism(newAgent.specie.tag));
        }
        if (agentsCreated<totalAgentsOnInit || specie.nrAlive == 0)
        {
            // On init, make the agents instantly adultAge old
            newAgent.lifetime = specie.adultAge;
        }
        // Create unique name for this behavior and assign it to the agent
        string behaviorName = CreateBehaviorName(newAgent);
        newAgent.SetBehaviorName(behaviorName);
        newAgent.Spawn(position);
    }


    public float GetAvgMoveForce(string tag)
    {
        float moveForce = 0;
        int counter = 0;
        foreach(AgentManager agent in Agents)
        {
            if (agent.gameObject.tag == tag)
            {
                moveForce += agent.moveForce;
                counter += 1;
            }
        }
        return moveForce/counter;
    }

    public float GetAvgMetabolism(string tag)
    {
        float metabolism = 0;
        int counter = 0;
        foreach(AgentManager agent in Agents)
        {
            if (agent.gameObject.tag == tag)
            {
                metabolism += agent.metabolism;
                counter += 1;
            }
        }
        return metabolism/counter;
    }

    public float GetAvgRotationForce(string tag)
    {
        float rotationForce = 0;
        int counter = 0;
        foreach(AgentManager agent in Agents)
        {
            if (agent.gameObject.tag == tag)
            {
                rotationForce += agent.rotationForce;
                counter += 1;
            }
        }
        return rotationForce/counter;
    }

    public float Mutate(float property)
    {
        return property + property * (UnityEngine.Random.value-0.5f) * mutationConstant;
    }

    
    public void RemoveAgent(AgentManager agent)
    {
        Agents.Remove(agent);
        agent.GetComponent<Collider>().enabled = false;
        // Destroy game object
        Destroy(agent.gameObject);
    }

    public Vector3 GetRandomSpawnPos()
    {
        Vector3 randomPosition = new Vector3(
                (UnityEngine.Random.value-0.5f) * boardSize,
                1.2f,
                (UnityEngine.Random.value-0.5f) * boardSize
             );
        randomPosition.y = Terrain.activeTerrain.SampleHeight(randomPosition);

        return randomPosition;
    }

    public String CreateBehaviorName(AgentManager agent)
    {
        // for example: sheep_PPO_3-5_16
        // a sheep trained with PPO and parents with id 3 and 5 and this sheep has id 16, i.e the 16th sheep to be born in the env.
        string specie = agent.specie.specieName;
        string trainingModel = agent.specie.trainingModel;
        string id = agent.id.ToString();
        string parentIDs = agent.parents[0] + "-" + agent.parents[1];
        return specie + "_" + trainingModel + "_" + parentIDs + "_" + id;
    }


    // When two agents successfully reproduces
    public void GiveBirthSexual(Specie specie, Vector3 position)
    {
        // Check if divisable by 2
        if (onlyBirthOnEvenNumbers % 2 == 0)
        {
            position = GetClosestFreePosition(position);
            InitializeAgent(specie, position);
        }
        // Add on to nr of attemtps
        onlyBirthOnEvenNumbers += 1;
        return;
    }

    // For each animat of a specie, there is a reproduction_probability of giving birth to a new animat
    public void GiveBirthAsexual(Specie specie, int nr_of_animats)
    {
        if (ecosystem1)
        {
            if (specie.tag=="prey")
            {
                for (int i=0; i<nr_of_animats; i++)
                {
                    if (UnityEngine.Random.value < specie.reproductionProbability*(1-nr_of_animats/preyCarryingCapacity))
                    {
                        Vector3 pos = GetClosestFreePosition(GetRandomSpawnPos());
                        InitializeAgent(specie, pos);
                    }
                }
            }
            else
            {
                for (int i=0; i<nr_of_animats; i++)
                {
                    if (UnityEngine.Random.value < specie.reproductionProbability)
                    {
                        Vector3 pos = GetClosestFreePosition(GetRandomSpawnPos());
                        InitializeAgent(specie, pos);
                    }
                }
            }
        }
        else
        {
            for (int i=0; i<nr_of_animats; i++)
            {
                if (UnityEngine.Random.value < specie.reproductionProbability)
                {
                    Vector3 pos = GetClosestFreePosition(GetRandomSpawnPos());
                    InitializeAgent(specie, pos);
                }
            }
        }

    }

    // Takes a position and finds a nearby (x,y) position which is not occupied, to avoid things from spawning upon each other. Loops through a number of tests, if one test fails (position is occupied) generate a neaby position and retry until we pass all tests.
    public Vector3 GetClosestFreePosition(Vector3 position)
    {
        // How far away should we retry if fail?
        float retryRadius = 5;

        // First check is position is non occupied by other agents
        foreach (Agent animat in Agents)
        {
            if ((animat.gameObject.transform.position - position).magnitude < 2)
            {
                Vector2 translation = UnityEngine.Random.insideUnitCircle * retryRadius;
                position.x += translation.x;
                position.z += translation.y;
                position.y = 1.2f;
                // Retry with new pos
                GetClosestFreePosition(position);
            }
        }
        // Check with all food objects
        foreach (FoodManager food in foodSpawner.foodList)
        {
            if ((food.transform.position - position).magnitude < 2)
            {
                Vector2 translation = UnityEngine.Random.insideUnitCircle * retryRadius;
                position.x += translation.x;
                position.z += translation.y;
                position.y = 1.8f;
                // Retry with new pos
                GetClosestFreePosition(position);
            }
        }

        // Check for all water objects
        // TODO Needs to be done in other way if water is not a point like source

        foreach (WaterManager water in waterSpawner.waterList)
        {
            int water_radius = 10;
            if ((water.transform.position - position).magnitude < water_radius)
            {
                Vector2 translation = UnityEngine.Random.insideUnitCircle * retryRadius;
                position.x += translation.x;
                position.z += translation.y;
                position.y = 1.2f;
                // Retry with new pos
                GetClosestFreePosition(position);
            }
        }

        // ! Additional obstacletypes need to be manually added here.

        // passed all checks, return the position
        return position;
    }

    public Tuple<int, int, int, int> CountIndividuals()
    {
        nPrey = 0;
        nPreyAdult = 0;
        nPredators = 0;
        nPredatorsAdult = 0;
        foreach (AgentManager animat in Agents)
        {
            if (animat.specie.specieName == "deer")
            {
                nPrey += 1;
                bool homeostaticsOk = true;
                if (animat.lifetime > animat.specie.adultAge)
                {
                    foreach (float homeostatic in animat.homeostaticValues)
                    {
                        if (homeostatic < homeostaticBirthThreshold)
                        {
                            homeostaticsOk = false;
                        }
                    }
                    if (homeostaticsOk)
                    {
                        nPreyAdult += 1;
                    }
                }
            }
            if (animat.specie.specieName == "wolf")
            {
                nPredators += 1;
                bool homeostaticsOk = true;
                if (animat.lifetime > animat.specie.adultAge)
                {
                    foreach (float homeostatic in animat.homeostaticValues)
                    {
                        if (homeostatic < homeostaticBirthThreshold)
                        {
                            homeostaticsOk = false;
                        }
                    }
                    if (homeostaticsOk)
                    {
                        nPredatorsAdult += 1;
                    }
                }
            }
        }
        return Tuple.Create(nPrey, nPreyAdult, nPredators, nPredatorsAdult);
    }

    public void FixedUpdate()
    {   
        nFixedUpdateFrames += 1;
        // send_to_python_counter += 1;
    
        // TODO Better solution?
        // To set foodSpawner and waterSpawner in not possible at awake.
        if (!spawnerSet)
        {
            foodSpawner = GetComponentInChildren<FoodSpawner>();
            waterSpawner = GetComponentInChildren<WaterSpawner>();
            simInfo = GetComponentInChildren<SimulationInfoCanvas>();
            spawnerSet = true;
        }

        
        (nPrey, nPreyAdult, nPredators, nPredatorsAdult) = CountIndividuals();
        
        nWater = GameObject.FindGameObjectsWithTag("water").Length;
        nFood = GameObject.FindGameObjectsWithTag("food").Length;
        m_Recorder.Add("Predators", nPredators);
        m_Recorder.Add("Prey", nPrey);
        m_Recorder.Add("Predators adult", nPredatorsAdult);
        m_Recorder.Add("Prey adult", nPreyAdult);
        m_Recorder.Add("Grass", nFood);
        m_Recorder.Add("Adult ratio prey", GetAdultRatio("prey"));
        m_Recorder.Add("Adult ratio predators", GetAdultRatio("predator"));
        m_Recorder.Add("Average age prey", GetAvgAge("prey"));
        m_Recorder.Add("Average age predator", GetAvgAge("predator"));
        m_Recorder.Add("Cumulative average nr prey killed in lifetime", GetAvgFood("predator"));
        m_Recorder.Add("Cumulative average nr grass eaten in lifetime", GetAvgFood("prey"));
        m_Recorder.Add("Prey reproduction probability", Species[0].reproductionProbability);
        m_Recorder.Add("Predator reproduction probability", Species[1].reproductionProbability);
        m_Recorder.Add("Food spawn probability", foodSpawner.food1.spawnFrequency);
        m_Recorder.Add("Probability of predation", GetRiskOfPredation());
        m_Recorder.Add("Probability of prey starving", GetRiskOfStarvationPrey());
        m_Recorder.Add("Probability of predation5", GetRiskOfPredation5());
        m_Recorder.Add("Probability of prey5 starving", GetRiskOfStarvationPrey5());
        m_Recorder.Add("Probability of predation6", GetRiskOfPredation6());
        m_Recorder.Add("Probability of prey6 starving", GetRiskOfStarvationPrey6());
        m_Recorder.Add("Steps", nFixedUpdateFrames);
        m_Recorder.Add("Metabolism", Species[0].metabolism);
        m_Recorder.Add("Prey speed", Species[0].moveForce);
        m_Recorder.Add("Predator speed", Species[1].moveForce);
        m_Recorder.Add("Avg speed prey", GetAvgMoveForce("prey"));
        m_Recorder.Add("Avg speed predator", GetAvgMoveForce("predator"));
        m_Recorder.Add("Avg speed prey", GetAvgMoveForce("prey"));
        m_Recorder.Add("Avg metabolism prey", GetAvgMetabolism("prey"));
        m_Recorder.Add("Avg metabolism predator", GetAvgMetabolism("predator"));

        if (reproduceAsexually)
        {
            GiveBirthAsexual(Species[0], nPreyAdult);
            GiveBirthAsexual(Species[1], nPredatorsAdult);
        }
        // if (send_to_python_counter == send_to_python_every)
        // {
        //     toPython.SendAreaInfoToPython(nPredators, nPrey, nFood, nWater, avgPreyLifetime, avgPredatorLifeTime, avgFoodCollected, avgPreyKilled);
        //     // Reset counter
        //     send_to_python_counter = 0;
        // }
        
        Species[0].nrAlive = nPrey;
        Species[1].nrAlive = nPredators;
        // To make sure not extinct
        if (cantGoExtinct)
        {
            if (nPredators==0 && nPredatorsAdult==0)
            {
                Vector3 vec = GetClosestFreePosition(GetRandomSpawnPos());
                InitializeAgent(Species[1], vec);
                // Bugfix, otherwise it will create multiple
                nPredators = 1;
            }
            if (nPrey==0 && nPreyAdult==0)
            {
                Vector3 vec = GetClosestFreePosition(GetRandomSpawnPos());
                InitializeAgent(Species[0], vec);
                // Bugfix, otherwise it will create multiple
                nPrey = 1;
            }
        }
        if (nPrey != 0 && nPreyAdult == 0)
        {
            foreach (AgentManager animat in Agents)
            {   
                if (animat.specie.specieName == "deer")
                {
                    animat.lifetime = animat.specie.adultAge;
                    break;
                }
            }
        }
        if (nPredators != 0 && nPredatorsAdult == 0)
        {
            foreach (AgentManager animat in Agents)
            {
                // If we have predators, but non is adult, make one of them adult
                if (animat.specie.specieName == "wolf")
                {
                    animat.lifetime = animat.specie.adultAge;
                    break;
                }
            }
        }

    
        simInfo.SetNumberOfAgents(nPredators, nPrey);
        simInfo.SetNumberOfGrass(nFood);
        simInfo.SetIterationNumber(nFixedUpdateFrames);
        
    }

    private void OnDestroy()
    {
        // if (Academy.IsInitialized) {
        //     SideChannelsManager.UnregisterSideChannel(toPython);
        // }
    }
    public float GetAdultRatio(string tag)
    {
        float adults = 0;
        float counter = 0;
        foreach(AgentManager agent in Agents)
        {
            if (agent.gameObject.tag == tag)
            {
                if (agent.lifetime > agent.specie.adultAge)
                {
                    adults += 1;
                }
                counter += 1;
            }
        }
        if (counter>0)
        {
            return (adults/counter);
        }
        else
        {
            return 0;
        }
    }

    public float GetRiskOfPredation()
    {
        if (predatedPrey>0 || starvedPrey>0)
        {
            // Minus since on init they are counted twice
            return predatedPrey/(predatedPrey+starvedPrey);
        }
        else
        {
            return 0;
        }
    }
    public float GetRiskOfStarvationPrey()
    {
        if (predatedPrey>0 || starvedPrey>0)
        {
            // Minus since on init they are counted twice
            return starvedPrey/(predatedPrey+starvedPrey);
        }
        else
        {
            return 0;
        }
    }
        public float GetRiskOfPredation5()
    {
        if (predatedPrey5>0 || starvedPrey5>0)
        {
            // Minus since on init they are counted twice
            return predatedPrey5/(predatedPrey5+starvedPrey5);
        }
        else
        {
            return 0;
        }
    }
    public float GetRiskOfStarvationPrey5()
    {
        if (predatedPrey5>0 || starvedPrey5>0)
        {
            // Minus since on init they are counted twice
            return starvedPrey5/(predatedPrey5+starvedPrey5);
        }
        else
        {
            return 0;
        }
    }
        public float GetRiskOfPredation6()
    {
        if (predatedPrey6>0 || starvedPrey6>0)
        {
            // Minus since on init they are counted twice
            return predatedPrey6/(predatedPrey6+starvedPrey6);
        }
        else
        {
            return 0;
        }
    }
    public float GetRiskOfStarvationPrey6()
    {
        if (predatedPrey6>0 || starvedPrey6>0)
        {
            // Minus since on init they are counted twice
            return starvedPrey6/(predatedPrey6+starvedPrey6);
        }
        else
        {
            return 0;
        }
    }

    public float GetAvgAge(string tag)
    {
        float lifetime = 0;
        float counter = 0;
        foreach(AgentManager agent in Agents)
        {
            if (agent.gameObject.tag == tag)
            {
                lifetime += agent.lifetime;
                counter += 1;
            }
        }
        if (counter>0)
        {
            return (lifetime/counter);
        }
        else
        {
            return 0;
        }
    }

    public float GetAvgFood(string tag)
    {
        float foodFreq = 0;
        int counter = 0;
        foreach(AgentManager agent in Agents)
        {
            if (agent.gameObject.tag == tag)
            {
                foodFreq += agent.GetFoodFreq();
                counter += 1;
            }
        }
        // return averages
        if (counter>0)
        {
            return (foodFreq/counter);
        }
        else
        {
            return 0;
        }
    }

}

