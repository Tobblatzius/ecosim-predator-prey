using System;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Policies;
using Unity.MLAgents.Sensors;
using UnityEngine;
using UnityEngine.AI;
using System.Collections;

/// <summary>
/// Controls a food object
/// </summary>
public class FoodManager : MonoBehaviour
{
    public FoodSpawner foodSpawner;

    public Food foodType;

    public AreaManager areaManager;

    public bool foodSpawnerSet = false;

    public void Initialize()
    {
        foodSpawner = GetComponentInParent<FoodSpawner>();
        areaManager = GetComponentInParent<AreaManager>();
    }

    // A foodobject has a position and a foodType property which describes this type of food specie.
    public void SetParameters(Vector3 pos, Food food)
    {
        transform.position = pos;
        foodType = food;
    }

    // Spawn an agent
    public void Spawn(Vector3 pos)
    {
        transform.position = pos;
    }

    public void GiveEnergyNeighborDeers(AgentManager animat_eating)
    {
        foreach(GameObject gameObject in GameObject.FindGameObjectsWithTag("prey"))
        {
            AgentManager animat = gameObject.GetComponent<AgentManager>();
            // If a deer
            if (animat.specie.specieName == "deer" )
            {
                // If within observaitonradius
                if (Vector3.Distance(animat_eating.transform.position, animat.transform.position) < 5)
                {
                    // Also give energy (sharing the prey)
                    animat.homeostaticValues[0] = Math.Min(animat.homeostaticValues[0] + 0.5f, 1);
                }
               
            }
        }
    }
    public void OnTriggerEnter(Collider collider)
    {
        // Check if colliding object is in the list of animats that can eat it.
        if (foodType.eatingActionNeeded)
        {
            if (collider.gameObject.tag == "eatingBubble")
            {
                AgentManager animat = collider.GetComponentInParent<AgentManager>();

                if (foodType.canEatIt.Contains(animat.gameObject.tag))
                {
                    animat.homeostaticValues[0] = Math.Min(animat.homeostaticValues[0] + foodType.energyGain, 1);
                    GiveEnergyNeighborDeers(animat);
                    Animator animation = animat.GetComponentInParent<Animator>();
                    animation.Play("eat");
                    // Remove this food 
                    Kill();
                }
            }
        }
        // If only collision is needed
        else
        {
            if (foodType.canEatIt.Contains(collider.gameObject.tag))
            {
                AgentManager animat = collider.GetComponentInParent<AgentManager>();
                animat.homeostaticValues[0] = Math.Min(animat.homeostaticValues[0] + foodType.energyGain, 1);
                animat.foodFreq += 1;
                animat.AddReward(animat.specie.eatReward);
                // GiveEnergyNeighborDeers(animat);
                Kill();
                // Animator animation = animat.GetComponentInParent<Animator>();
                // animation.Play("eat");
                // Remove this food 
                
            }
            if (collider.gameObject.tag == "water")
            {
                Kill();
            }
        }

    }
    public void Kill()
    {
        foodSpawner.RemoveFood(this);
    }
    public void FixedUpdate()
    {
        // This I tried to have in awake/start method, but then the FoodSpawner script was not available for the FoodManager. A bit inefficient to keep checking this each iteration.
        // TODO Better solution?
        if (!foodSpawnerSet)
        {
            foodSpawner = GetComponentInParent<FoodSpawner>();
            foodSpawnerSet = true;
        }
    }
}
