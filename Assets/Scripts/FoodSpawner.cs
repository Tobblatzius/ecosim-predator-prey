using System.Collections.Generic;
using UnityEngine;
using System;
using Unity.MLAgents;
using Unity.MLAgents.SideChannels;
using System.Linq;
using UnityEngine.AI;


[Serializable]
public class Food
{
    [Tooltip("How many of this food type that is spawned on init.")]
    public int nrOnInit;

    [Tooltip("The energyGain a animat which can eat this food type obtains when it eats it.")]
    public float energyGain;

    [Tooltip("The gametags which can eat this food object")]
    public List<string> canEatIt;

    [Tooltip("Each second there is a spawnFrequency chance of spawning the food")]
    public float spawnFrequency;

    public GameObject prefab;

    [Tooltip("Whether or not an eating action is needed to eat this food type")]
    public bool eatingActionNeeded;
}

public class FoodSpawner : MonoBehaviour
{
    [SerializeField]
    public Food food1 = new Food();

    public Food food2 = new Food();

    public bool staticNrOfFood;

    protected AreaManager areaManager;


    [Tooltip("List of different food types in the area")]
    public List<Food> foodTypeList = new List<Food>();

    [Tooltip("List of active food objects in the area")]
    public List<FoodManager> foodList = new List<FoodManager>();


    // Bake the navmesh for each surface specified in Navmeshbaker


    // Need to initialize on awake as well, otherwise food would not exist and episode would end
    void Start()
    {
        areaManager = GetComponentInParent<AreaManager>();
        AddFoodTypesToList();
        InitializeFoods();
    }
    // Check if any food has been eaten and add new

    // Add food types to the list so that spawners now which different types it needs to check for.
    // Note that this needs to be set manually!
    private void AddFoodTypesToList()
    {
        foodTypeList.Add(food1);
        foodTypeList.Add(food2);
    }

    // Initializes one food
    public void InitializeFood(Food food, Vector3 position)
    {
        FoodManager newFood = Instantiate(food.prefab, transform).GetComponent<FoodManager>();
        foodList.Add(newFood);
        newFood.SetParameters(position, food);
        newFood.Spawn(position);
    }

    // Initialize nrOnInit foodobjects
    public void InitializeFoods()
    {
        // For each food type, create nroninit food objects of this type.
        foreach (Food food in foodTypeList)
        {
            while (foodList.Count < food.nrOnInit)
            {
                Vector3 spawnpos = GetRandomSpawnPos();
                spawnpos.y = 2;
                InitializeFood(food, spawnpos);
            }
        }
    }

    // Checks what to do at each fixed update
    public void FoodAreaController()
    {
        if (staticNrOfFood)
        {
            // Do nothing, the remove food function makes sure that each time objects gets removed, a new one is spawned
            return;
        }
        else
        {
            // Spawn food object of type food with probability given by the food type.
            foreach (Food food in foodTypeList)
            {
                RandomSpawn(food);
            }
            
        }
    }


    public void RandomSpawn(Food food)
    {
        foreach(FoodManager f in foodList.ToList())
        {
            float random = UnityEngine.Random.value;
            // divide by 60 since ca this many frames per second
            if (random < food.spawnFrequency*(1-foodList.Count/areaManager.foodCarryingCapacity))
            {
                Vector3 spawnpos = GetRandomSpawnPos();
                spawnpos = areaManager.GetClosestFreePosition(spawnpos);
                InitializeFood(food, spawnpos);
            }
        }
        if (foodList.Count==0)
        {
            Vector3 spawnpos = GetRandomSpawnPos();
            spawnpos = areaManager.GetClosestFreePosition(spawnpos);
            InitializeFood(food, spawnpos);
        }
    }

    // Find random position  within the envRadius, fixed spawn height.
    public Vector3 GetRandomSpawnPos()
    {
        Vector3 randomPosition = new Vector3(
                (UnityEngine.Random.value-0.5f) * areaManager.boardSize,
                0,
                (UnityEngine.Random.value-0.5f) * areaManager.boardSize
             );
        // Fix above ground
        randomPosition.y = 2;
        return randomPosition;
    }
    public void RemoveFood(FoodManager food)
    {
        // get the specie of the food object we want to remove
        Food foodType = food.foodType;
        // Remnove it from the list of food objects
        foodList.Remove(food);
        // Remove it from the simulation
        Destroy(food.gameObject);
        // If we want a static nr of food, create new food object of same specie.
        if (staticNrOfFood)
        {
            Vector3 spawnpos = GetRandomSpawnPos();
            spawnpos = areaManager.GetClosestFreePosition(spawnpos);
            InitializeFood(foodType, spawnpos);
        }
    }
    public void FixedUpdate()
    {
        FoodAreaController();
    }
}
