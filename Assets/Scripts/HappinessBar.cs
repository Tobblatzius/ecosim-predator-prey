using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HappinessBar : MonoBehaviour
{
    [Tooltip("The slider that should be attached to this Health Bar object.")]
    public Slider slider;

    /// <summary>
    /// Sets the slider that in turn controlls the value of the health bar. 
    /// </summary>
    /// <param name="value">The value to set bar to</param>
    public void SetHappinessBar(float value)
    {
        slider.value = value;
    }
}