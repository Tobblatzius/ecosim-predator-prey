﻿using System;
using Unity.MLAgents.SideChannels;
using System.Collections.Generic;

/// <summary>
/// This Side Channel is used to send information about the simulation to the python script. Both 
/// information about the simulation in general and information from each agent is supported 
/// through this channel and what kind of informatino that is sent is indicated by an int in the 
/// beginning of each message. 
/// </summary>
public class InfoToPythonSideChannel : SideChannel
{
    public InfoToPythonSideChannel()
    {
        ChannelId = new Guid("d3785fc4607fe74abe4a6fa103ccf59b");

    }

    /// <summary>
    /// This function must be implemented in a side channel to deal with incomming messages. No 
    /// message should be sent to this side channel, thus we leave this empty. 
    /// </summary>
    /// <param name="msg">The incomming message</param>
    protected override void OnMessageReceived(IncomingMessage msg) { }

    /// <summary>
    /// Used by a single agent to send information about its current state. 
    /// </summary>
    /// <param name="agentName">The name of the agent</param>
    /// <param name="agentHomeostasis">A list of the homeostasic variables to be sent</param>
    public void SendAgentInfoToPython(string agentName, List<float> agentHomeostasis)
    {
        using (OutgoingMessage msgOut = new OutgoingMessage()) {
            msgOut.WriteInt32(0);  // Signals the type of information
            msgOut.WriteString(agentName);
            msgOut.WriteFloatList(agentHomeostasis);
            QueueMessageToSend(msgOut);
        }
    }

    /// <summary>
    /// Used by the environment to send general information about the simulation. 
    /// </summary>
    /// <param name="nPredator">The current number of predators</param>
    /// <param name="nGrassEater">The current number of grass eaters</param>
    /// <param name="nGoodFood">The current number of good food</param>
    /// <param name="nBadFood">The current number of bad food</param>
    public void SendAreaInfoToPython(int nPredator, int nPrey, int nGoodFood, int nWater, float avgPreyLifetime, float avgPredatorLifeTime, float avgFoodCollected, float avgPreyKilled)
    {
        using (OutgoingMessage msgOut = new OutgoingMessage()) {
            msgOut.WriteInt32(1);  // Signals the type of information
            msgOut.WriteInt32(nPredator);
            msgOut.WriteInt32(nPrey);
            msgOut.WriteInt32(nGoodFood);
            msgOut.WriteInt32(nWater);
            msgOut.WriteString(avgPreyLifetime.ToString());
            msgOut.WriteString(avgPredatorLifeTime.ToString());
            msgOut.WriteString(avgFoodCollected.ToString());
            msgOut.WriteString(avgPreyKilled.ToString());
            QueueMessageToSend(msgOut);
        }
    }
}