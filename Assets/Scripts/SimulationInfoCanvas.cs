using UnityEngine;
using UnityEngine.UI;

public class SimulationInfoCanvas : MonoBehaviour
{
    public Text nPredators;
    public Text nPrey;
    public Text nGrass;
    public Text step;

    private string predatorPresentation = "Predators: ";
    private string preyPresentation = "Prey: ";
    private string grassPresentation = "Grass: ";
    private string stepPresentation = "Step: ";

    public void SetNumberOfAgents(int nPredators, int nPrey)
    {
        this.nPredators.text = predatorPresentation + nPredators.ToString();
        this.nPrey.text = preyPresentation + nPrey.ToString();
    }

    public void SetNumberOfGrass(int nGrass)
    {
        this.nGrass.text = grassPresentation + nGrass.ToString();

    }

    public void SetIterationNumber(int step)
    {
        this.step.text = stepPresentation + step.ToString();
    }
}

