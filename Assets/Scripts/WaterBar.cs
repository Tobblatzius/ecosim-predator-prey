using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaterBar : MonoBehaviour
{
    [Tooltip("The slider that should be attached to this Health Bar object.")]
    public Slider slider;

    public void SetWaterBar(float value)
    {
        slider.value = value;
    }
}