using System;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Policies;
using Unity.MLAgents.Sensors;
using UnityEngine;
using UnityEngine.AI;


/// <summary>
/// Controls a water object
/// </summary>
public class WaterManager : MonoBehaviour
{
    public WaterSpawner waterSpawner;

    public Water waterType;

    public bool waterSpawnerSet = false;

    public void Initialize()
    {
        waterSpawner = GetComponentInParent<WaterSpawner>();
    }

    // A waterobject has a position and a waterType property which describes this type of water specie.
    public void SetParameters(Vector3 pos, Water water)
    {
        transform.position = pos;
        waterType = water;
    }

    // Spawn an agent
    public void Spawn(Vector3 pos)
    {
        transform.position = pos;
    }

    void OnTriggerEnter(Collider collider)
    {
        // Check if colliding object is in the list of animats that can eat it.
        if (waterType.eatingActionNeeded)
        {
            if (collider.gameObject.tag == "eatingBubble")
            {
                AgentManager animat = collider.GetComponentInParent<AgentManager>();

                if (waterType.canEatIt.Contains(animat.gameObject.tag))
                {
                    if (animat.homeostaticValues.Length > 1)
                    {

                        animat.homeostaticValues[1] = Math.Min(animat.homeostaticValues[1] + waterType.waterGain, 1);
                    }
                }
            }
        }
        // If only collision is needed
        else
        {
            if (waterType.canEatIt.Contains(collider.gameObject.tag))
            {
                AgentManager animat = collider.GetComponentInParent<AgentManager>();
                // Assuming that "water" is on second index in homeostatic array
                animat.isDrinking = true;                
                if (animat.homeostaticValues.Length > 1)
                {
                    animat.homeostaticValues[1] = Math.Min(animat.homeostaticValues[1] + waterType.waterGain, 1);
                }
            }
        }
    }

    void OnTriggerExit(Collider collider)
    {
        AgentManager animat = collider.GetComponentInParent<AgentManager>();
        animat.isDrinking = false;
    }



    public void Kill()
    {
        waterSpawner.RemoveWater(this);
    }

    public void FixedUpdate()
    {
        // This I tried to have in awake/start method, but then the WaterSpawner script was not available for the WaterManager. A bit inefficient to keep checking this each iteration.
        // TODO Better solution?
        if (!waterSpawnerSet)
        {
            waterSpawner = GetComponentInParent<WaterSpawner>();
            waterSpawnerSet = true;
        }
    }
}