using System.Collections.Generic;
using UnityEngine;
using System;
using Unity.MLAgents;
using Unity.MLAgents.SideChannels;
using System.Linq;
using UnityEngine.AI;


[Serializable]
public class Water
{
    [Tooltip("How many of this water type that is spawned on init.")]
    public int nrOnInit;

    [Tooltip("Spawnpositions of the nrOnInit water objects.")]
    public List<Vector3> spawnPositions = new List<Vector3>();

    [Tooltip("The energyGain a animat which can eat this water type obtains when it eats it.")]
    public float waterGain;

    [Tooltip("If the position at spawn should be random.")]
    public bool randomSpawn;

    [Tooltip("The gametags which can eat this water object")]
    public List<string> canEatIt;

    [Tooltip("Each second there is a spawnFrequency chance of spawning the water")]
    public float spawnFrequency;

    public GameObject prefab;

    [Tooltip("Whether or not an eating action is needed to eat this water type")]
    public bool eatingActionNeeded;

}

public class WaterSpawner : MonoBehaviour
{
    [SerializeField]
    public Water water = new Water();

    public bool staticNrOfWater;

    protected AreaManager areaManager;

    [Tooltip("List of different water types in the area")]
    public List<Water> waterTypeList = new List<Water>();

    [Tooltip("List of active water objects in the area")]
    public List<WaterManager> waterList = new List<WaterManager>();


    void Start()
    {
        areaManager = GetComponentInParent<AreaManager>();
        AddWaterTypesToList();
        InitializeWaters();
    }

    // Add water types to the list so that spawners now which different types it needs to check for.
    // Note that this needs to be set manually!
    private void AddWaterTypesToList()
    {
        waterTypeList.Add(water);

    }

    // Initializes one water source
    public void InitializeWater(Water water, Vector3 position)
    {
        WaterManager newWater = Instantiate(water.prefab, transform).GetComponent<WaterManager>();
        waterList.Add(newWater);
        newWater.SetParameters(position, water);
        newWater.Spawn(position);
    }

    // Initialize nrOnInit waterobjects
    public void InitializeWaters()
    {
        // For each water type, create nroninit water objects of this type.
        
        foreach (Water water in waterTypeList)
        {
            int i = 0;
            while (waterList.Count < water.nrOnInit)
            {
                if (water.randomSpawn)
                {
                    InitializeWater(water, GetRandomSpawnPos());
                }
                else
                {
                    Vector3 spawnpos;
                    spawnpos.y = 0.5f;
                    spawnpos.x = water.spawnPositions[i].x;
                    spawnpos.z = water.spawnPositions[i].z;
                    InitializeWater(water, spawnpos);
                    i = i+1;
                }
            }
        }
    }

    // Find random position  within the envRadius, fixed spawn height.
        public Vector3 GetRandomSpawnPos()
    {
        Vector3 randomPosition = new Vector3(
                (UnityEngine.Random.value-0.5f) * areaManager.boardSize,
                0,
                (UnityEngine.Random.value-0.5f) * areaManager.boardSize
             );
        // Fix above ground
        randomPosition.y = 0.5f;
        return randomPosition;
    }
    public void RemoveWater(WaterManager water)
    {
        // get the specie of the water object we want to remove
        Water waterType = water.waterType;
        // Remnove it from the list of water objects
        waterList.Remove(water);
        // Remove it from the simulation
        Destroy(water.gameObject);
        // If we want a static nr of water, create new water object of same specie.
        if (staticNrOfWater)
        {
            Vector3 spawnpos = GetRandomSpawnPos();
            spawnpos = areaManager.GetClosestFreePosition(spawnpos);
            InitializeWater(waterType, spawnpos);
        }
    }
}
