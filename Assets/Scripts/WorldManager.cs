﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// The WorldManager builds the environment. Useful if we want several independent environments in the "world".
/// </summary>
public class WorldManager : MonoBehaviour
{

    [Tooltip("The prefab used for the environment.")]
    public GameObject environmentPrefab;
    // public float demoDeltaTime;
    // [Tooltip("Default is 0.02, if set at 0.01, the game runs at twice the speed, if 0.002, then ten times the speed.")]
    // public float trainingDeltaTime;
    // public bool runAtDemoSpeed;

    private void Awake()
    {
        // if (runAtDemoSpeed) {
        //     Time.fixedDeltaTime = demoDeltaTime;
        // } else {
        //     Time.fixedDeltaTime = trainingDeltaTime;
        // }
        BuildEnvironment(environmentPrefab);
    }

    // Builds an environment from terrainPrefab
    private void BuildEnvironment(GameObject terrainPrefab)
    {
        GameObject environment = Instantiate(environmentPrefab, transform);
    }

}
