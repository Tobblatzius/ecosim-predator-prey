# EcosimLotkaVolterra

## What is this?
Simulated ecosystem with multiple agents trained through Reinforcement Learning. Will the dynamic be in accordance with the Lotka Volterra equations for predator-prey models and competitive systems?

## Folder structure
In Ecosim/Training, the RL code is.  
In Ecosim/Assets/Scripts, are the Unity scripts.  

## How to run
You can either run in the editor (run python script and press play in Unity) or in headless-mode.

### Unity-editor  
To train agents. Run python_trainer.py in Ecosim/Training and press play when the scene has been selected in Unity.  
If python_trainer is not run and you simply push play in the Unity editor, you will control each animat with wasd-controllers on your keyboard.

### Headless-mode
If you want to train the models on say colab, you need to make a headless build and set the .x86_64 as unity_file_name in the python_trainer.
To make a build, in the editor go to File->Build Settings and use this setting and click on build.
![alt text](images/unitybuild.png)

## Monitoring
You can monitor the training using tensorboard. Go to the folder where the tensorboard data is being stored and run
```
tensorboard dev upload --logdir . 
```

I have slightly edited what is being monitored. You can make changes in Ecosim/Training/ecosimmodules/tensorboard_modifier.py to edit what you want to write on a step.

## Recordings
Following https://learn.unity.com/tutorial/working-with-the-unity-recorder-2019-3. Using this setting
![alt text](images/unityrecorder.png)

## Architecture
Coming later...



